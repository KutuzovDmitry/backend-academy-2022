package ru.tinkoff.academy.hashtree;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Optional;
import java.util.Set;
import ru.tinkoff.academy.hashtree.minheap.BinaryHeap;
import ru.tinkoff.academy.hashtree.minheap.MinHeap;

/**
 * Формулировка задачи из статьи из Хабра, приложенной нам к заданию: Реализовать структуру данных,
 * в которой операция поиска элемента на основании равенства выполняется с асимптотикой O(1), а
 * операции вставки и удаления работали с учётом операций и сравнения и равенства, и строили
 * бинарное дерево таким образом, что наименьший элемент был бы корнем дерева.
 * <p>
 * Мне же не понравилась данная статья, так как если в дереве поиска наименьший элемент - корень
 * дерева то оно будет не сбалансированным. Для данной задачи лучше подойдет MinHeap + HashSet.
 * </p>
 */
public class HashTree<T extends Comparable<T>> implements Collection<T>, BinaryHeap<T> {

    private final Set<T> set = new HashSet<>();
    private final MinHeap<T> minHeap = new MinHeap<>();

    @Override
    public boolean contains(Object o) {
        return set.contains(o);
    }

    @Override
    public Optional<T> getMin() {
        return minHeap.getMin();
    }

    @Override
    public Optional<T> getMax() {
        return minHeap.getMax();
    }

    @Override
    public boolean add(T value) {
        if (contains(value)) {
            return false;
        }

        set.add(value);
        minHeap.add(value);
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        return c.stream().map(this::add).reduce(true, (x, y) -> x & y);
    }

    @Override
    public boolean remove(Object o) {
        if (!contains(o)) {
            return false;
        }
        set.remove(o);
        minHeap.remove(o);
        return true;
    }

    @Override
    public boolean remove(T value) {
        return remove((Object) value);
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return c.stream().map(this::remove).reduce(true, (x, y) -> x & y);
    }

    @Override
    public void clear() {
        set.clear();
        minHeap.clear();
    }

    @Override
    public int size() {
        return minHeap.size();
    }

    @Override
    public boolean isEmpty() {
        return minHeap.isEmpty();
    }

    @Override
    public Iterator<T> iterator() {
        return minHeap.iterator();
    }

    @Override
    public Object[] toArray() {
        return minHeap.toArray();
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return minHeap.containsAll(c);
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException();
    }
}

package ru.tinkoff.academy.hashtree.minheap;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

public class MinHeap<T extends Comparable<T>> implements BinaryHeap<T>, Collection<T> {

    private final List<T> heap = new ArrayList<>();

    @Override
    public Optional<T> getMin() {
        return size() == 0 ? Optional.empty() : Optional.of(heap.get(0));
    }

    @Override
    public Optional<T> getMax() {
        return heap.stream().max(Comparable::compareTo);
    }

    private void swapElements(int index1, int index2) {
        T tmp = heap.get(index1);
        heap.set(index1, heap.get(index2));
        heap.set(index2, tmp);
    }

    private static int parentIndex(int childIndex) {
        return (childIndex - 1) / 2;
    }

    /**
     * Restores heap properties starting from index to the root of heap.
     *
     * @param index of broken node.
     */
    private void siftUp(int index) {
        while (heap.get(index).compareTo(heap.get(parentIndex(index))) < 0) {
            swapElements(index, parentIndex(index));
            index = parentIndex(index);
        }
    }

    @Override
    public boolean add(T value) {
        heap.add(value);
        siftUp(heap.size() - 1);
        return true;
    }

    @Override
    public boolean remove(Object object) {
        throw new UnsupportedOperationException();
    }

    private static int getRightChildIndex(int index) {
        return 2 * index + 2;
    }

    private static int getLeftChildIndex(int index) {
        return 2 * index + 1;
    }

    /**
     * Restores heap properties from index to leaf.
     *
     * @param index of broken node.
     */
    private void siftDown(int index) {
        while (getLeftChildIndex(index) < heap.size()) {
            int left = getLeftChildIndex(index);
            int right = getRightChildIndex(index);
            int minChild = left;
            if (right < heap.size() &&
                heap.get(right).compareTo(heap.get(left)) < 0) {
                minChild = right;
            }
            if (heap.get(index).compareTo(heap.get(minChild)) <= 0) {
                break;
            }
            swapElements(index, minChild);
            index = minChild;
        }
    }

    @Override
    public boolean remove(T value) {
        int index = heap.indexOf(value);
        if (index == -1) {
            return false;
        }
        swapElements(index, heap.size() - 1);
        heap.remove(heap.size() - 1);
        siftDown(index);
        return true;
    }

    @Override
    public int size() {
        return heap.size();
    }

    @Override
    public boolean isEmpty() {
        return heap.isEmpty();
    }

    @Override
    public boolean contains(Object object) {
        return heap.contains(object);
    }

    @Override
    public Iterator<T> iterator() {
        return heap.iterator();
    }

    @Override
    public Object[] toArray() {
        return heap.toArray();
    }

    @Override
    public boolean containsAll(Collection<?> collection) {
        return new HashSet<>(heap).containsAll(collection);
    }

    @Override
    public boolean addAll(Collection<? extends T> collection) {
        collection.forEach(this::add);
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        return collection.stream()
            .map(this::remove)
            .reduce(true, (x, y) -> x & y);
    }

    @Override
    public void clear() {
        heap.clear();
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        throw new UnsupportedOperationException();
    }

    @Override
    public <T1> T1[] toArray(T1[] array) {
        throw new UnsupportedOperationException();
    }
}

package ru.tinkoff.academy.hashtree.minheap;

import java.util.Optional;

/**
 * Binary heap interface.
 * <p>
 * Depending on implementation can return min or max in O(1)
 * </p>
 *
 * @param <T> Type of values in heap
 */
public interface BinaryHeap<T extends Comparable<T>> {

    /**
     * @return minimum from heap in O(1) or O(n) (depending on type of heap)
     */
    Optional<T> getMin();

    /**
     * @return maximum from heap in O(1) or O(n) (depending on type of heap)
     */
    Optional<T> getMax();

    /**
     * Add element in O(log(n))
     *
     * @param value to add
     * @return true (required by Collections interface)
     */
    boolean add(T value);

    /**
     * Remove elements in O(log(n))
     *
     * @param value to remove
     * @return true if collection was changed.
     */
    boolean remove(T value);
}

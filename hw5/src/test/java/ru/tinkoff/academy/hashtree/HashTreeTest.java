package ru.tinkoff.academy.hashtree;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class HashTreeTest {

    HashTree<Integer> hashTree;

    @BeforeEach
    void setUp() {
        hashTree = new HashTree<>();
    }

    @Test
    void contains() {
        assertFalse(hashTree.contains(-1));
        hashTree.add(-1);
        assertTrue(hashTree.contains(-1));
    }

    @Test
    void getMin() {
        hashTree.add(10);
        hashTree.add(-1);
        hashTree.add(3);
        hashTree.add(4);
        assertEquals(-1, hashTree.getMin().orElse(0));
    }

    @Test
    void getMax() {
        hashTree.add(10);
        hashTree.add(-1);
        hashTree.add(3);
        hashTree.add(4);
        assertEquals(10, hashTree.getMax().orElse(0));
    }

    @Test
    void add() {
        assertEquals(0, hashTree.size());
        hashTree.add(10);
        assertEquals(1, hashTree.size());
    }

    @Test
    void remove() {
        hashTree.add(10);
        hashTree.add(-1);
        hashTree.add(3);
        hashTree.add(4);
        hashTree.remove(10);
        assertEquals(3, hashTree.size());
    }

    @Test
    void clear() {
        hashTree.add(10);
        hashTree.add(-1);
        hashTree.add(3);
        hashTree.add(4);
        assertEquals(4, hashTree.size());
        hashTree.clear();
        assertEquals(0, hashTree.size());
    }

    @Test
    void size() {
        assertEquals(0, hashTree.size());
        hashTree.add(10);
        assertEquals(1, hashTree.size());
        hashTree.add(-1);
        assertEquals(2, hashTree.size());
        hashTree.add(3);
        assertEquals(3, hashTree.size());
        hashTree.add(4);
        assertEquals(4, hashTree.size());
    }

    @Test
    void isEmpty() {
        assertTrue(hashTree.isEmpty());
        hashTree.add(10);
        assertFalse(hashTree.isEmpty());
    }
}
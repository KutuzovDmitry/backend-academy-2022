package ru.tinkoff.academy.hashtree.minheap;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class MinHeapTest {

    MinHeap<Integer> minHeap;

    @BeforeEach
    void setMinHeap() {
        minHeap = new MinHeap<>();
    }

    @Test
    void getMin() {
        minHeap.add(10);
        minHeap.add(-1);
        minHeap.add(3);
        assertEquals(-1, minHeap.getMin().get());
    }

    @Test
    void size() {
        assertEquals(0, minHeap.size());
        minHeap.add(-1);
        minHeap.add(-2);
        minHeap.add(-2);
        assertEquals(3, minHeap.size());
    }

    @Test
    void add() {
        minHeap.add(0);
        minHeap.add(0);
        minHeap.add(1);
        assertEquals(3, minHeap.size());
    }

    @Test
    void remove() {
        minHeap.add(-1);
        minHeap.remove(-1);
        assertEquals(0, minHeap.size());

        minHeap.add(-1);
        minHeap.add(0);
        minHeap.add(10);
        minHeap.add(4);
        minHeap.remove(-1);
        assertEquals(0, minHeap.getMin().get());
    }
}
package ru.tinkoff.academy.task4;

public class Main {
    public static void main(String[] args) {
        Printer printerOne = new Printer();
        Printer printerTwo = new Printer();
        Printer printerThree = new Printer();

        printerOne.setPrintableChar('1');
        printerOne.setNextChar('2');

        printerTwo.setPrintableChar('2');
        printerTwo.setNextChar('3');

        printerThree.setNextChar('1');
        printerThree.setPrintableChar('3');

        Thread threadPrinterOne = new Thread(printerOne.getPrinterTask());
        Thread threadPrinterTwo = new Thread(printerTwo.getPrinterTask());
        Thread threadPrinterThree = new Thread(printerThree.getPrinterTask());

        threadPrinterOne.start();
        threadPrinterTwo.start();
        threadPrinterThree.start();
    }
}

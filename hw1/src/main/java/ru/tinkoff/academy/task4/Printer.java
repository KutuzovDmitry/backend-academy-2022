package ru.tinkoff.academy.task4;

public class Printer {
    private static final int COUNT_OF_CHARS = 6;
    private static final Object LOCKER = new Object();
    private static final char LAST_CHAR = '3';
    private int sealsCount = 0;
    private char printableChar;
    private char nextChar;
    static char requiredChar = '1';

    public void setPrintableChar(char printableChar) {
        this.printableChar = printableChar;
    }

    public void setNextChar(char nextChar) {
        this.nextChar = nextChar;
    }

    public Runnable getPrinterTask() {
        return () -> {
            while (sealsCount < COUNT_OF_CHARS) {
                synchronized (LOCKER) {
                    while (requiredChar != printableChar) {
                        notifyNextPrinter();
                    }
                    print();
                    notifyNextPrinter();
                }
            }
        };
    }

    private void notifyNextPrinter() {
        LOCKER.notifyAll();
        try {
            if (!isLastChar())
                LOCKER.wait();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void print() {
        System.out.print(printableChar);
        requiredChar = nextChar;
        sealsCount++;
    }

    private boolean isLastChar() {
        return printableChar == LAST_CHAR && sealsCount == COUNT_OF_CHARS;
    }
}


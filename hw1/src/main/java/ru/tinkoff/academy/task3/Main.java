package ru.tinkoff.academy.task3;

import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;

public class Main {
    private static final int STORE_CAPACITY = 10;
    private static final int MAX_RANDOM_VALUE = 20;
    private static final int MIN_PRODUCER_SLEEP_IN_MILLIS = 1000;

    private static ArrayBlockingQueue<Integer> store = new ArrayBlockingQueue<>(STORE_CAPACITY);

    public static void main(String[] args) {
        ProducerThread producerThread = new ProducerThread();
        producerThread.start();

        ConsumerThread consumerThread = new ConsumerThread();
        consumerThread.start();
    }

    private static class ProducerThread extends Thread {
        @Override
        public void run() {
            try {
                while (true) {
                    int randomValue = new Random().nextInt(MAX_RANDOM_VALUE);
                    System.out.println(randomValue + " поступает на склад");
                    Thread.sleep(randomValue + MIN_PRODUCER_SLEEP_IN_MILLIS);
                    System.out.println(randomValue + " поставлен на склад");
                    store.offer(randomValue);
                }
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private static class ConsumerThread extends Thread {
        @Override
        public void run() {
            try {
                while (true) {
                    System.out.println("Покупатель ожидает получения товара");
                    Integer recievedItem = null;
                    recievedItem = store.take();
                    System.out.println("Покупатель получил товар " + recievedItem);
                }
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}

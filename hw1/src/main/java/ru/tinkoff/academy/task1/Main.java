package ru.tinkoff.academy.task1;

import java.util.List;

public class Main {
    public static List<Integer> squareNotEndingInFiveOrSix(List<Integer> list) {
        return list.stream()
                .map(x -> x * x + 10)
                .filter(x -> x % 10 != 5 && x % 10 != 6)
                .toList();
    }

    public static void main(String[] args) {
        List<Integer> x = List.of(1, 2, 3, 4, 5, 6, 7);
        System.out.println(squareNotEndingInFiveOrSix(x));
    }
}

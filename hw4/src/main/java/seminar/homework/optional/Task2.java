package seminar.homework.optional;

import java.util.Collections;
import seminar.User;

import java.util.List;
import java.util.Optional;

public class Task2 {
    private static final String ADMIN_ROLE = "admin";

    /*
    * Заменил стремный if на orElse + findAny
    */
    public Optional<User> findAnyAdmin() {
        return findUsersByRole(ADMIN_ROLE)
            .orElse(Collections.emptyList())
            .stream().findAny();
    }

    private Optional<List<User>> findUsersByRole(String role) {
        //real search in DB
        return Optional.empty();
    }



}

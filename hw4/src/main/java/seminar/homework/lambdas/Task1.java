package seminar.homework.lambdas;

import seminar.Permission;
import seminar.User;

import java.util.HashSet;
import java.util.Set;

public class Task1 {
    private final Set<User> users = new HashSet<>();

    /*
    * Для данной цели хорошо подходит метод removeIf
    */
    public void removeUsersWithPermission(Permission permission) {
        users.removeIf(user -> hasUserPermission(permission, user));
    }

    /*
    * Выделил в отдельный метод, чтобы не было двойного уровня вложности лямбд
    */
    private static boolean hasUserPermission(Permission permission, User user) {
        return user.getRoles()
                    .stream()
                    .anyMatch(role -> role.getPermissions().contains(permission));
    }
}
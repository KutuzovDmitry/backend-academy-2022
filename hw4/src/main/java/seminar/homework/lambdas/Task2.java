package seminar.homework.lambdas;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import seminar.User;

public class Task2 {

    private final Map<String, Set<User>> usersByRole = new HashMap<>();

    /*
     * В данном случае хорошо подойдет метод computeIfAbsent, где мы вернем новую сету,
     * если она отсутствует по ключу
     */
    public void addUser(User user) {
        user.getRoles().forEach(role -> usersByRole
                .computeIfAbsent(role.getName(), name -> new HashSet<>())
                .add(user));
    }

    /*
     * Для такого случая существует getOrDefault
     */
    public Set<User> getUsersInRole(String role) {
        return usersByRole.getOrDefault(role, Collections.emptySet());
    }

}

package seminar.homework.streams;

import seminar.User;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Task2 {
    private final Set<User> users = new HashSet<>();

    /*
    * Заменил reduce на mapToInt + sum
    */
    public int getTotalAge() {
        return users.stream()
                .mapToInt(User::getAge)
                .sum();
    }

    /*
    * Заменил flatMap в User на mapToInt + sum
    */
    public int countEmployees(Map<String, List<User>> departments) {
        return (int) departments.values().stream()
                .mapToInt(List::size)
                .sum();
    }
}

package seminar.homework.streams;

import static java.util.stream.Collectors.toMap;

import java.util.List;
import java.util.Map;
import seminar.User;

public class Task1 {

    /*
     * Заменил groupingBy на Collectors.toMap(), с merge функцией на Integer::max
     */
    public Map<String, Integer> getMaxAgeByUserName(List<User> users) {
        return users.stream()
            .collect(toMap(User::getName, User::getAge, Integer::max));
    }
}

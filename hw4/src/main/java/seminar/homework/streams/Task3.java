package seminar.homework.streams;


import java.util.IntSummaryStatistics;
import seminar.User;

import java.util.List;
import java.util.stream.IntStream;

public class Task3 {
    /*
    * Заменил два вызова min и max на двух разных стримах на IntSummaryStatistics
    */
    public void printNameStats(List<User> users) {
        IntSummaryStatistics stats = getNameLengthStream(users).summaryStatistics();
        System.out.println("MAX: " + stats.getMax());
        System.out.println("MIN: " + stats.getMin());
    }

    private IntStream getNameLengthStream(List<User> users) {
        return users.stream()
                .mapToInt(user -> user.getName().length());
    }
}

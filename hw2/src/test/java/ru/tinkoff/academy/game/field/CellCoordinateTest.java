package ru.tinkoff.academy.game.field;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class CellCoordinateTest {

    @Test
    void moveShouldMoveCoordinateInGivenDirection() {
        CellCoordinate start = new CellCoordinate(0, 0);
        CellCoordinate direction = new CellCoordinate(-1, 5);
        CellCoordinate moved = CellCoordinate.move(start, direction);
        Assertions.assertEquals(direction.x + start.y, moved.x);
        Assertions.assertEquals(direction.y + start.y, moved.y);
    }

    @Test
    void doubledShouldDoubleVector() {
        CellCoordinate start = new CellCoordinate(1, 1);
        CellCoordinate doubled = CellCoordinate.doubled(start);
        Assertions.assertEquals(start.x * 2, doubled.x);
        Assertions.assertEquals(start.y * 2, doubled.y);
    }
}
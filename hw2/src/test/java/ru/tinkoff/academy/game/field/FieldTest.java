package ru.tinkoff.academy.game.field;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static ru.tinkoff.academy.game.field.CellValue.CROSS;
import static ru.tinkoff.academy.game.field.CellValue.NONE;
import static ru.tinkoff.academy.game.field.CellValue.ZERO;

import java.util.Optional;
import java.util.stream.Stream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class FieldTest {

    private static final int TEST_FIELD_SIZE = 3;
    Field field;

    @BeforeEach
    void setUp() {
        field = new Field(TEST_FIELD_SIZE);
    }

    @ParameterizedTest
    @MethodSource("getDrawFields")
    void isDrawShouldReturnTrueOnDraw(CellValue[][] valuesForDraw) {
        field.setField(valuesForDraw);
        assertTrue(field.isDraw());
    }

    @ParameterizedTest
    @MethodSource("getWinFields")
    void isDrawShouldReturnFalseOnDraw(CellValue[][] valuesForWin) {
        field.setField(valuesForWin);
        assertFalse(field.isDraw());
    }

    @ParameterizedTest
    @MethodSource("getDrawFields")
    void isFieldFullShouldReturnTrueOnFilledField(CellValue[][] fullFilledField) {
        field.setField(fullFilledField);
        assertTrue(field.isFieldFull());
    }

    @ParameterizedTest
    @MethodSource("getFieldsDuringGame")
    void isFieldFullShouldReturnFalseIfAnyNone(CellValue[][] fieldWithNone) {
        field.setField(fieldWithNone);
        assertFalse(field.isFieldFull());
    }

    @ParameterizedTest
    @MethodSource("getWinFields")
    void hasWinnerShouldReturnTrueIfWinnerExists(CellValue[][] valuesForWin) {
        field.setField(valuesForWin);
        assertTrue(field.hasWinner());
    }

    @ParameterizedTest
    @MethodSource("getDrawFields")
    void hasWinnerShouldReturnFalseIfNoWinner(CellValue[][] fullFilledField) {
        field.setField(fullFilledField);
        assertFalse(field.hasWinner());
    }

    @ParameterizedTest
    @MethodSource("getWinFields")
    void getWinnerSideShouldReturnWinnerIfPresent(CellValue[][] valuesForWin) {
        field.setField(valuesForWin);
        assertEquals(CROSS, field.getWinnerSide().orElse(NONE));
    }

    @ParameterizedTest
    @MethodSource("getDrawFields")
    void getWinnerSideShouldReturnEmptyIfWinnerAbsent(CellValue[][] fullFilledField) {
        field.setField(fullFilledField);
        assertEquals(Optional.empty(), field.getWinnerSide());
    }

    @ParameterizedTest
    @MethodSource("getWinFields")
    void hasWinnerAtShouldReturnTrueOnWinnersCell(CellValue[][] fieldWithWinner) {
        field.setField(fieldWithWinner);
        assertTrue(field.hasWinnerAt(0, 0));
    }

    @ParameterizedTest
    @MethodSource("getDrawFields")
    void hasWinnerAtShouldReturnFalseOnDrawField(CellValue[][] drawField) {
        field.setField(drawField);
        for (int y = 0; y < TEST_FIELD_SIZE; ++y) {
            for (int x = 0; x < TEST_FIELD_SIZE; ++x) {
                assertFalse(field.hasWinnerAt(x, y));
            }
        }
    }

    @Test
    void isValidCoordinatesShouldReturnTrueWithCoordsInField() {
        for (int y = 0; y < TEST_FIELD_SIZE; ++y) {
            for (int x = 0; x < TEST_FIELD_SIZE; x++) {
                assertTrue(field.isValidCoordinates(x, y));
            }
        }
    }

    @Test
    void isValidCoordinatesShouldReturnFalseOnInvalidCoordinates() {
        assertFalse(field.isValidCoordinates(TEST_FIELD_SIZE, TEST_FIELD_SIZE));
    }

    @ParameterizedTest
    @MethodSource("getDrawFields")
    void getCellValueShouldReturnValueFromField(CellValue[][] drawField) {
        field.setField(drawField);
        assertEquals(drawField[0][0], field.getCellValue(0, 0));
    }

    public static Stream<Arguments> getDrawFields() {
        return Stream.of(
            Arguments.of(
                (Object) new CellValue[][]{
                    {CROSS, ZERO, CROSS},
                    {CROSS, ZERO, CROSS},
                    {ZERO, CROSS, ZERO}}
            )
        );
    }

    public static Stream<Arguments> getWinFields() {
        return Stream.of(
            Arguments.of(
                (Object) new CellValue[][]{
                    {CROSS, NONE, ZERO},
                    {NONE, CROSS, ZERO},
                    {NONE, NONE, CROSS}
                }
            )
        );
    }

    public static Stream<Arguments> getFieldsDuringGame() {
        return Stream.of(
            Arguments.of(
                (Object) new CellValue[][]{
                    {CROSS, ZERO, CROSS},
                    {CROSS, ZERO, CROSS},
                    {ZERO, CROSS, NONE}
                }
            )
        );
    }
}
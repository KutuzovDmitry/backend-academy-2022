package ru.tinkoff.academy.game.player;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static ru.tinkoff.academy.game.field.CellValue.CROSS;
import static ru.tinkoff.academy.game.field.CellValue.NONE;
import static ru.tinkoff.academy.game.field.CellValue.ZERO;

import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import ru.tinkoff.academy.game.field.CellCoordinate;
import ru.tinkoff.academy.game.field.CellValue;
import ru.tinkoff.academy.game.field.Field;

class BotTest {

    Player bot;

    @ParameterizedTest
    @MethodSource("obviousCases")
    void botShouldGuessObviousCases(Field field, CellValue botSide, CellCoordinate nextMove) {
        bot = new Bot(field, botSide);
        assertEquals(nextMove, bot.askPlayerForCell());
    }

    @ParameterizedTest
    @MethodSource("remainingCases")
    void botShouldChooseRemainingPosition(Field field, CellValue botSide, CellCoordinate nextMove) {
        bot = new Bot(field, botSide);
        assertEquals(nextMove, bot.askPlayerForCell());
    }

    private static Stream<Arguments> remainingCases() {
        return Stream.of(
            Arguments.of(
                new Field(new CellValue[][]{
                    {CROSS, NONE, ZERO},
                    {ZERO, CROSS, CROSS},
                    {CROSS, ZERO, ZERO}
                }),
                CROSS,
                new CellCoordinate(1, 0)
            ),
            Arguments.of(
                new Field(new CellValue[][]{
                    {CROSS, NONE, ZERO},
                    {CROSS, CROSS, ZERO},
                    {ZERO, ZERO, CROSS}
                }),
                ZERO,
                new CellCoordinate(1, 0)
            ),
            Arguments.of(
                new Field(new CellValue[][]{
                    {CROSS, CROSS, ZERO},
                    {ZERO, ZERO, CROSS},
                    {CROSS, ZERO, NONE}
                }),
                CROSS,
                new CellCoordinate(2, 2)
            )
        );
    }

    private static Stream<Arguments> obviousCases() {
        return Stream.of(
            Arguments.of(
                new Field(new CellValue[][]{
                    {CROSS, NONE, ZERO},
                    {NONE, CROSS, ZERO},
                    {NONE, NONE, NONE}
                }),
                CROSS,
                new CellCoordinate(2, 2)
            ),
            Arguments.of(
                new Field(new CellValue[][]{
                    {CROSS, NONE, ZERO},
                    {CROSS, NONE, ZERO},
                    {NONE, NONE, NONE}
                }),
                ZERO,
                new CellCoordinate(2, 2)
            ),
            Arguments.of(
                new Field(new CellValue[][]{
                    {CROSS, CROSS, NONE},
                    {ZERO, ZERO, NONE},
                    {NONE, NONE, NONE}
                }),
                CROSS,
                new CellCoordinate(2, 0)
            )
        );
    }
}
package ru.tinkoff.academy.game;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static ru.tinkoff.academy.game.field.CellValue.CROSS;
import static ru.tinkoff.academy.game.field.CellValue.ZERO;

import java.util.stream.Stream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import ru.tinkoff.academy.game.field.CellCoordinate;
import ru.tinkoff.academy.game.field.CellValue;
import ru.tinkoff.academy.game.field.Field;
import ru.tinkoff.academy.game.player.HumanPlayer;
import ru.tinkoff.academy.game.player.Player;

class GameTest {

    public static final CellValue FIRST_MOCK_SIDE = CROSS;
    public static final CellValue SECOND_MOCK_SIDE = ZERO;
    private static final CellCoordinate FIRST_MOCK_CHOICE = new CellCoordinate(1, 1);
    private static final CellCoordinate SECOND_MOCK_CHOICE = new CellCoordinate(1, 2);
    public static final int FIELD_SIZE = 3;
    Game game;
    Field fieldToTest;
    Player player1 = Mockito.mock(HumanPlayer.class);
    Player player2 = Mockito.mock(HumanPlayer.class);

    @BeforeEach
    void setUp() {
        fieldToTest = new Field(FIELD_SIZE);
        game = new Game(fieldToTest, player1, player2);
    }

    @Test
    void isRunningShouldReturnTrueOnStart() {
        assertTrue(game.isRunning());
    }

    @ParameterizedTest
    @MethodSource("getDrawFields")
    void isRunningShouldReturnFalseOnDraw(CellValue[][] drawField) {
        fieldToTest.setField(drawField);
        assertFalse(game.isRunning());
    }

    @Test
    void updateShouldUpdateFieldState() {
        Mockito.when(player1.getSide()).thenReturn(FIRST_MOCK_SIDE);
        Mockito.when(player1.askPlayerForCell()).thenReturn(FIRST_MOCK_CHOICE);
        game.update();
        assertEquals(FIRST_MOCK_SIDE, fieldToTest.getCellValue(FIRST_MOCK_CHOICE));

        Mockito.when(player2.getSide()).thenReturn(SECOND_MOCK_SIDE);
        Mockito.when(player2.askPlayerForCell()).thenReturn(SECOND_MOCK_CHOICE);
        game.update();
        assertEquals(SECOND_MOCK_SIDE, fieldToTest.getCellValue(SECOND_MOCK_CHOICE));
    }

    public static Stream<Arguments> getDrawFields() {
        return Stream.of(
            Arguments.of(
                (Object) new CellValue[][]{
                    {CROSS, ZERO, CROSS},
                    {CROSS, ZERO, CROSS},
                    {ZERO, CROSS, ZERO}}
            )
        );
    }
}
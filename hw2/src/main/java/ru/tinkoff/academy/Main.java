package ru.tinkoff.academy;

import java.util.Scanner;
import ru.tinkoff.academy.game.Game;
import ru.tinkoff.academy.game.field.CellValue;
import ru.tinkoff.academy.game.field.Field;
import ru.tinkoff.academy.game.player.Bot;
import ru.tinkoff.academy.game.player.HumanPlayer;
import ru.tinkoff.academy.game.player.Player;
import ru.tinkoff.academy.game.utils.FieldPrinter;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int fieldSize = askPlayerForFieldSize(scanner);
        Field field = new Field(fieldSize);
        FieldPrinter fieldPrinter = new FieldPrinter(field);

        Player player1 = new HumanPlayer(CellValue.CROSS, "Dima", scanner);
        Player player2 = new Bot(field, CellValue.ZERO);

        Game game = new Game(field, player1, player2);
        while (game.isRunning()) {
            fieldPrinter.printField();
            game.update();
        }
        game.printWinner();
        scanner.close();
    }

    private static int askPlayerForFieldSize(Scanner scanner) {
        System.out.println("Please enter field size value [Example: <1..N>]:");
        return scanner.nextInt();
    }
}
package ru.tinkoff.academy.game.field;

public enum CellValue {
    NONE(' '), CROSS('X'), ZERO('O');

    private char symbol;

    CellValue(char symbol) {
        this.symbol = symbol;
    }

    public char getSymbol() {
        return symbol;
    }

    public boolean isNotEmpty() {
        return this.symbol != ' ';
    }

    public boolean isEmpty() {
        return this.symbol == ' ';
    }
}


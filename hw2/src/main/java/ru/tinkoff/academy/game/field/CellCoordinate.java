package ru.tinkoff.academy.game.field;

import lombok.Data;

@Data
public class CellCoordinate {

    public final int x;
    public final int y;

    public static CellCoordinate move(CellCoordinate start, CellCoordinate direction) {
        return new CellCoordinate(start.x + direction.x, start.y + direction.y);
    }

    public static CellCoordinate doubled(CellCoordinate coordinate) {
        return new CellCoordinate(coordinate.x * 2, coordinate.y * 2);
    }
}

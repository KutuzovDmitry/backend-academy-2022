package ru.tinkoff.academy.game.player;

import java.util.Scanner;
import ru.tinkoff.academy.game.field.CellCoordinate;
import ru.tinkoff.academy.game.field.CellValue;

public class HumanPlayer implements Player {

    private CellValue side;
    private String name;
    private Scanner scanner;

    public HumanPlayer(CellValue side, String name, Scanner scanner) {
        this.side = side;
        this.name = name;
        this.scanner = scanner;
    }

    @Override
    public CellCoordinate askPlayerForCell() {
        System.out.println(
            "Your turn, " + name + "! Choose your cell [Example: <0...N-1> <0...N-1>]:");
        int x = scanner.nextInt();
        int y = scanner.nextInt();
        return new CellCoordinate(x, y);
    }

    @Override
    public CellValue getSide() {
        return this.side;
    }

    @Override
    public String getName() {
        return name;
    }
}

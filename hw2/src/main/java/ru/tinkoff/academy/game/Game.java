package ru.tinkoff.academy.game;

import java.util.Optional;
import ru.tinkoff.academy.game.field.CellCoordinate;
import ru.tinkoff.academy.game.field.CellValue;
import ru.tinkoff.academy.game.field.Field;
import ru.tinkoff.academy.game.player.Player;

public class Game {

    private Field field;
    private Player[] players;
    private int currentTurn = 0;

    private Optional<Player> winner;

    public Game(Field field, Player player1, Player player2) {
        this.field = field;
        players = new Player[2];
        players[0] = player1;
        players[1] = player2;
        winner = Optional.empty();
    }

    public boolean isRunning() {
        return winner.isEmpty() && !field.isDraw();
    }

    public void printWinner() {
        String winner = this.winner.map(x -> x.getName() + " won!")
                                    .orElse("Draw!");
        System.out.println(winner);
    }

    /**
     * Update game state and ask players for move
     */
    public void update() {
        Player currentPlayer = players[currentTurn];
        CellCoordinate chosenCell = askValidCellCoordinates(currentPlayer);
        field.setCellValue(chosenCell, currentPlayer.getSide());
        this.winner = getWinner();
        updateTurn();
    }

    /**
     * Ask player for valid cell coordinates
     */
    private CellCoordinate askValidCellCoordinates(Player currentPlayer) {
        CellCoordinate chosenCell = currentPlayer.askPlayerForCell();
        while (field.isNotValidCoordinates(chosenCell) || field.cellIsNotEmpty(chosenCell)) {
            System.out.println("Please write valid coordinate [Example: <1...N> <1...N>]:");
            chosenCell = currentPlayer.askPlayerForCell();
        }
        return chosenCell;
    }

    private Optional<Player> getWinner() {
        return field.getWinnerSide()
            .map(this::getPlayerBySide);
    }

    private Player getPlayerBySide(CellValue side) {
        if (players[0].getSide().equals(side)) {
            return players[0];
        }
        return players[1];
    }

    private void updateTurn() {
        currentTurn = (currentTurn + 1) % 2;
    }
}

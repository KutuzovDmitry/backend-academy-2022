package ru.tinkoff.academy.game.player;

import ru.tinkoff.academy.game.field.CellCoordinate;
import ru.tinkoff.academy.game.field.CellValue;

public interface Player {

    CellCoordinate askPlayerForCell();

    CellValue getSide();

    String getName();
}

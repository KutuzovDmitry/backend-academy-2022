package ru.tinkoff.academy.game.utils;

import ru.tinkoff.academy.game.field.Field;

public class FieldPrinter {

    public static final String VERTICAL_SEPARATOR = "│";
    private static final String HORIZONTAL_SEPARATOR = "─";
    private static final String CROSSED_SEPARATOR = "┼";
    public static final String EMPTY_CELL = " ";

    private Field field;

    public FieldPrinter(Field field) {
        this.field = field;
    }

    /**
     * Prints game field in System.out
     */
    public void printField() {
        int fieldSize = field.getFieldSize();
        for (int y = 0; y < fieldSize; ++y) {
            for (int x = 0; x < fieldSize; ++x) {
                String separator = VERTICAL_SEPARATOR;
                if (x == fieldSize - 1) {
                    separator = EMPTY_CELL;
                }
                char cellSymbol = field.getCellValue(x, y).getSymbol();
                System.out.print(cellSymbol + separator);
            }
            System.out.println();
            String separator = getHorizontalSeparator();
            if (y != fieldSize - 1) {
                System.out.println(separator);
            }
        }
    }

    /**
     * Returns horizontal separator for printed field
     */
    private String getHorizontalSeparator() {
        int fieldSize = field.getFieldSize();
        StringBuilder separator = new StringBuilder();
        int sizeOfPrintableField = fieldSize * 2 - 1;
        for (int i = 0; i < sizeOfPrintableField; ++i) {
            if (i % 2 == 0) {
                separator.append(HORIZONTAL_SEPARATOR);
            } else {
                separator.append(CROSSED_SEPARATOR);
            }
        }
        return separator.toString();
    }
}

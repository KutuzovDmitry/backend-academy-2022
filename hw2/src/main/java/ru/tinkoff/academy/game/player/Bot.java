package ru.tinkoff.academy.game.player;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;
import ru.tinkoff.academy.game.field.CellCoordinate;
import ru.tinkoff.academy.game.field.CellValue;
import ru.tinkoff.academy.game.field.Field;

public class Bot implements Player {

    private static final int MAX_MINIMAX_DEPTH = 10;
    private final int STARTING_MINIMAX_DEPTH = -1;
    private Field currentGameField;
    private CellValue side;

    private record ScorePoint(int x, int y, int score) { }

    public Bot(Field field, CellValue side) {
        this.currentGameField = field;
        this.side = side;
    }

    @Override
    public CellCoordinate askPlayerForCell() {
        List<ScorePoint> scores = getScorePoints(currentGameField, side,
            STARTING_MINIMAX_DEPTH);
        ScorePoint scorePoint = scores.stream().max(Comparator.comparing(ScorePoint::score)).get();
        System.out.println("Bot chose " + scorePoint.x + " " + scorePoint.y);
        return new CellCoordinate(scorePoint.x, scorePoint.y);
    }

    @Override
    public CellValue getSide() {
        return side;
    }

    @Override
    public String getName() {
        return "Bot";
    }

    private int minimax(Field field, CellCoordinate coordinate, CellValue side, int depth) {
        field.setCellValue(coordinate, side);

        Optional<CellValue> winnerSide = field.getWinnerSide();
        if (winnerSide.isPresent() || depth > MAX_MINIMAX_DEPTH) {
            return side == getSide() ? 10 - depth : depth - 10;
        }

        CellValue newSide = switchTurn(side);
        List<ScorePoint> scores = getScorePoints(field, newSide, depth);
        if (scores.isEmpty()) {
            return 0;
        }

        IntStream scoreStream = scores.stream().mapToInt(ScorePoint::score);
        return side == getSide() ? scoreStream.max().getAsInt() : scoreStream.min().getAsInt();
    }

    private List<ScorePoint> getScorePoints(Field field, CellValue side, int depth) {
        depth++;
        List<ScorePoint> scorePoints = new ArrayList<>();

        for (int y = 0; y < field.getFieldSize(); ++y) {
            for (int x = 0; x < field.getFieldSize(); ++x) {
                Field fieldCopy = new Field(field);
                CellCoordinate currentCoordinate = new CellCoordinate(x, y);
                if (fieldCopy.getCellValue(currentCoordinate).isEmpty()) {
                    int score = minimax(fieldCopy, currentCoordinate, side, depth);
                    ScorePoint scorePoint = new ScorePoint(x, y, score);
                    scorePoints.add(scorePoint);
                }
            }
        }

        return scorePoints;
    }

    private CellValue switchTurn(CellValue side) {
        if (side.equals(CellValue.ZERO)) {
            return CellValue.CROSS;
        }
        return CellValue.ZERO;
    }
}

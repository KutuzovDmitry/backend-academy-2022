package ru.tinkoff.academy.game.field;

import static ru.tinkoff.academy.game.field.CellCoordinate.doubled;
import static ru.tinkoff.academy.game.field.CellCoordinate.move;
import static ru.tinkoff.academy.game.field.CellValue.NONE;

import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Stream;

public class Field {

    private final int fieldSize;
    private CellValue[][] field;
    private int emptyCells;

    public Field(int fieldSize) {
        this.fieldSize = fieldSize;
        initializeField(fieldSize);
        emptyCells = fieldSize * fieldSize;
    }

    private void initializeField(int fieldSize) {
        field = new CellValue[fieldSize][fieldSize];
        for (int i = 0; i < fieldSize; i++) {
            for (int j = 0; j < fieldSize; j++) {
                field[i][j] = NONE;
            }
        }
    }

    public Field(Field field) {
        this.fieldSize = field.getFieldSize();
        this.field = new CellValue[fieldSize][fieldSize];
        for (int i = 0; i < fieldSize; i++) {
            this.field[i] = Arrays.copyOf(field.field[i], fieldSize);
        }
        this.emptyCells = field.emptyCells;
    }

    public Field(CellValue[][] field) {
        this.fieldSize = field.length;
        setField(field);
    }

    /**
     * Checks if there is draw in game field
     */
    public boolean isDraw() {
        return !hasWinner() && isFieldFull();
    }

    public boolean isFieldFull() {
        return emptyCells == 0;
    }

    /**
     * Checks if there is a winner in game field
     */
    public boolean hasWinner() {
        return getWinnerSide().isPresent();
    }

    /**
     * Returns won side, if present
     */
    public Optional<CellValue> getWinnerSide() {
        for (int y = 0; y < fieldSize; ++y) {
            for (int x = 0; x < fieldSize; ++x) {
                if (hasWinnerAt(x, y)) {
                    CellValue currentCellValue = getCellValue(x, y);
                    return Optional.of(currentCellValue);
                }
            }
        }
        return Optional.empty();
    }

    /**
     * Checks if field has winner at concrete position
     */
    public boolean hasWinnerAt(int x, int y) {
        CellCoordinate start = new CellCoordinate(x, y);
        CellCoordinate[] directions = getDirections();
        for (CellCoordinate direction : directions) {
            boolean winnerFound = checkOnDirection(start, direction);
            if (winnerFound) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns direction for winner founding
     */
    private static CellCoordinate[] getDirections() {
        CellCoordinate[] directions = {
            new CellCoordinate(1, 0), // Horizontal direction
            new CellCoordinate(0, 1), // Vertical direction
            new CellCoordinate(1, 1), // Diagonal direction
            new CellCoordinate(-1, 1) // Another diagonal direction
        };
        return directions;
    }

    /**
     * Checks if there is a winner in the given direction
     */
    private boolean checkOnDirection(CellCoordinate start, CellCoordinate direction) {
        CellCoordinate movedCoordinates = move(start, direction);
        CellCoordinate doubleMovedCoordinates = move(start, doubled(direction));
        if (isNotValidCoordinates(doubleMovedCoordinates)) {
            return false;
        }
        CellValue firstValue = getCellValue(start);
        CellValue secondValue = getCellValue(movedCoordinates);
        CellValue thirdValue = getCellValue(doubleMovedCoordinates);
        return firstValue.equals(secondValue) && firstValue.equals(thirdValue)
            && firstValue.isNotEmpty();
    }

    /**
     * Checks the coordinates for validity in game field
     */
    public boolean isNotValidCoordinates(CellCoordinate coordinate) {
        int x = coordinate.x;
        int y = coordinate.y;
        return !isValidCoordinates(x, y);
    }

    /**
     * Checks the coordinates for validity in game field
     */
    public boolean isValidCoordinates(int x, int y) {
        return y >= 0 && y < fieldSize && x >= 0 && x < fieldSize;
    }

    public boolean cellIsNotEmpty(CellCoordinate coordinate) {
        return getCellValue(coordinate).isNotEmpty();
    }

    public int getFieldSize() {
        return fieldSize;
    }

    public void setCellValue(CellCoordinate coordinate, CellValue value) {
        int y = coordinate.y;
        int x = coordinate.x;
        field[y][x] = value;
        emptyCells--;
    }

    public CellValue getCellValue(int x, int y) {
        return field[y][x];
    }

    public CellValue getCellValue(CellCoordinate coordinate) {
        return getCellValue(coordinate.x, coordinate.y);
    }

    public CellValue[][] getField() {
        return field;
    }

    public void setField(CellValue[][] field) {
        emptyCells = (int)Arrays.stream(field)
            .flatMap(Stream::of)
            .filter(CellValue::isEmpty)
            .count();
        this.field = field;
    }
}

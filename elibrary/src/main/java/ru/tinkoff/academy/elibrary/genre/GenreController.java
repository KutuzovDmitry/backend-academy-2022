package ru.tinkoff.academy.elibrary.genre;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.tinkoff.academy.elibrary.genre.dto.CreatingGenreDto;

@RestController
@RequestMapping("/genres")
@RequiredArgsConstructor
public class GenreController {

    private final GenreService genreService;

    @PostMapping
    public Genre save(@RequestBody CreatingGenreDto creatingGenreDto) {
        return genreService.save(creatingGenreDto);
    }

    @GetMapping
    public List<Genre> getAll() {
        return genreService.getAll();
    }

    @GetMapping("/{id}")
    public Genre getById(@PathVariable Long id) {
        return genreService.getById(id);
    }

    @PutMapping("/{id}")
    public Genre update(@PathVariable Long id, @RequestBody CreatingGenreDto creatingGenreDto) {
        return genreService.update(id, creatingGenreDto);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable Long id) {
        genreService.removeById(id);
    }

}

package ru.tinkoff.academy.elibrary.publisher;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.tinkoff.academy.elibrary.publisher.dto.CreatingPublisherDto;


@RestController
@RequestMapping("/publishers")
@RequiredArgsConstructor
public class PublisherController {

    private final PublisherService publisherService;

    @GetMapping
    public List<Publisher> getAll() {
        return publisherService.getAll();
    }

    @GetMapping("/{id}")
    public Publisher getById(@PathVariable Long id) {
        return publisherService.getById(id);
    }

    @PostMapping
    public Publisher save(@RequestBody CreatingPublisherDto creatingPublisherDto) {
        return publisherService.save(creatingPublisherDto);
    }

    @PutMapping("/{id}")
    public Publisher update(@PathVariable Long id,
        @RequestBody CreatingPublisherDto creatingPublisherDto) {
        return publisherService.update(id, creatingPublisherDto);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        publisherService.deleteById(id);
    }
}

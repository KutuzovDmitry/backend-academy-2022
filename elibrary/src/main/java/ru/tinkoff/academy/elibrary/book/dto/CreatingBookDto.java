package ru.tinkoff.academy.elibrary.book.dto;


import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreatingBookDto {

    private String title;

    private Long genreId;

    private Long publisherId;

    private LocalDate publicationDate;
}

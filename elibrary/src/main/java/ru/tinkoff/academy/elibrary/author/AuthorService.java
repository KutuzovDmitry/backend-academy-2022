package ru.tinkoff.academy.elibrary.author;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import ru.tinkoff.academy.elibrary.author.dto.CreatingAuthorDto;
import ru.tinkoff.academy.elibrary.common.exceptions.ResponseException;

@Service
@RequiredArgsConstructor
public class AuthorService {

    private final AuthorRepository authorRepository;

    public Author getById(Long authorId) {
        return authorRepository.findById(authorId)
            .orElseThrow(() -> new ResponseException(HttpStatus.NOT_FOUND,
                "Author with id " + authorId + " not found"));
    }

    public List<Author> findAll() {
        return authorRepository.findAll();
    }

    public Author save(CreatingAuthorDto authorDto) {
        Author author = Author.builder()
            .name(authorDto.getName())
            .build();
        return authorRepository.save(author);
    }

    public void deleteById(Long authorId) {
        authorRepository.deleteById(authorId);
    }

    public Author update(Long id, CreatingAuthorDto creatingAuthorDto) {
        Author authorToUpdate = authorRepository.findById(id)
            .orElseThrow(() -> new ResponseException(HttpStatus.NOT_FOUND,
                "Author with id " + id + " not found"));

        authorToUpdate.setName(creatingAuthorDto.getName());

        return authorRepository.save(authorToUpdate);
    }

    public void deleteAll() {
        authorRepository.deleteAll();
    }
}

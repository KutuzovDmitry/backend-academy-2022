package ru.tinkoff.academy.elibrary.relations.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreatingAuthorBookDto {

    private Long bookId;

    private Long authorId;

}

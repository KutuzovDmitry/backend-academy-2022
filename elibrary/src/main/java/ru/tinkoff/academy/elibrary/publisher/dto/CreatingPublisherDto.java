package ru.tinkoff.academy.elibrary.publisher.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreatingPublisherDto {

    private String name;

    private Long addressId;
}

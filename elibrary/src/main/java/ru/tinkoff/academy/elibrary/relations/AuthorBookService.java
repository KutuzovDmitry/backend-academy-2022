package ru.tinkoff.academy.elibrary.relations;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import ru.tinkoff.academy.elibrary.common.exceptions.ResponseException;
import ru.tinkoff.academy.elibrary.relations.dto.CreatingAuthorBookDto;

@Service
@RequiredArgsConstructor
public class AuthorBookService {

    private final AuthorBookRepository authorBookRepository;

    public AuthorBook save(CreatingAuthorBookDto creatingAuthorBookDto) {
        AuthorBook authorBook = AuthorBook.builder()
            .authorId(creatingAuthorBookDto.getAuthorId())
            .bookId(creatingAuthorBookDto.getBookId())
            .build();

        return authorBookRepository.save(authorBook);
    }

    public AuthorBook getById(Long id) {
        return authorBookRepository.findById(id)
            .orElseThrow(() -> new ResponseException(HttpStatus.NOT_FOUND,
                "No relation AuthorBook with id " + id + " found"));
    }

    public List<AuthorBook> getAll() {
        return authorBookRepository.findAll();
    }

    public AuthorBook update(Long id, CreatingAuthorBookDto creatingAuthorBookDto) {
        AuthorBook authorBookToUpdate = authorBookRepository.findById(id)
            .orElseThrow(() -> new ResponseException(HttpStatus.NOT_FOUND,
                "No relation AuthorBook with id " + id + " found"));

        authorBookToUpdate.setAuthorId(creatingAuthorBookDto.getAuthorId());
        authorBookToUpdate.setBookId(creatingAuthorBookDto.getBookId());

        return authorBookRepository.save(authorBookToUpdate);
    }

    public void delete(Long id) {
        authorBookRepository.deleteById(id);
    }

}

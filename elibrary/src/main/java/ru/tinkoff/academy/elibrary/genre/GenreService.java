package ru.tinkoff.academy.elibrary.genre;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import ru.tinkoff.academy.elibrary.common.exceptions.ResponseException;
import ru.tinkoff.academy.elibrary.genre.dto.CreatingGenreDto;

@Service
@RequiredArgsConstructor
public class GenreService {

    private final GenreRepository genreRepository;

    public Genre getById(Long genreId) {
        return genreRepository.findById(genreId)
            .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                "Genre with id " + genreId + " not found"));
    }

    public List<Genre> getAll() {
        return genreRepository.findAll();
    }

    public Genre save(CreatingGenreDto genreDto) {
        Genre genreToSave = Genre.builder()
            .name(genreDto.getName())
            .build();
        return genreRepository.save(genreToSave);
    }

    public void removeById(Long genreId) {
        genreRepository.deleteById(genreId);
    }

    public Genre update(Long id, CreatingGenreDto creatingGenreDto) {
        Genre genreToUpdate = genreRepository.findById(id).orElseThrow(
            () -> new ResponseException(HttpStatus.NOT_FOUND,
                "Genre with id " + id + " not found"));

        genreToUpdate.setName(creatingGenreDto.getName());

        return genreRepository.save(genreToUpdate);
    }
}

package ru.tinkoff.academy.elibrary.book.specification;

import java.util.List;
import java.util.Map;
import javax.persistence.criteria.Predicate;
import org.springframework.data.jpa.domain.Specification;
import ru.tinkoff.academy.elibrary.book.Book;

public class BookSearchSpecification {

    public static Specification<Book> searchByParams(Map<String, String> params) {
        return (book, cq, cb) -> {
            List<Predicate> predicates = params.entrySet()
                .stream()
                .map(entry -> cb.equal(book.get(entry.getKey()), entry.getValue()))
                .toList();
            return cb.and(predicates.toArray(new Predicate[0]));
        };
    }

    public static Specification<Book> searchByTitle(String title) {
        return (book, cq, cb) -> cb.like(book.get("title"), "%" + title + "%");
    }


    public static Specification<Book> searchByAuthor(String author) {
        return (book, cq, cb) -> cb.like(book.join("authors").get("name"), "%" + author + "%");
    }

    public static Specification<Book> searchByGenre(String genre) {
        return (book, cq, cb) -> cb.like(book.join("genre").get("name"), "%" + genre + "%");
    }
}

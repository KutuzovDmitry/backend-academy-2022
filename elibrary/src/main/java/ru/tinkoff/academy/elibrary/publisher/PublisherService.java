package ru.tinkoff.academy.elibrary.publisher;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import ru.tinkoff.academy.elibrary.address.Address;
import ru.tinkoff.academy.elibrary.address.AddressService;
import ru.tinkoff.academy.elibrary.common.exceptions.ResponseException;
import ru.tinkoff.academy.elibrary.publisher.dto.CreatingPublisherDto;

@Service
@RequiredArgsConstructor
public class PublisherService {

    private final PublisherRepository publisherRepository;
    private final AddressService addressService;


    public Publisher save(CreatingPublisherDto publisherDto) {
        Address address = addressService.getById(publisherDto.getAddressId());
        if (address.getPublisher() != null) {
            throw new ResponseException(HttpStatus.BAD_REQUEST, "Publisher with such addressId already exists");
        }

        Publisher publisherToCreate = Publisher.builder()
            .name(publisherDto.getName())
            .address(address)
            .build();

        return publisherRepository.save(publisherToCreate);
    }

    public Publisher getById(Long id) {
        return publisherRepository.findById(id)
            .orElseThrow(() -> new ResponseException(HttpStatus.NOT_FOUND,
                "Publisher with id " + id + " not found"));
    }

    public List<Publisher> getAll() {
        return publisherRepository.findAll();
    }

    public Publisher update(Long id, CreatingPublisherDto creatingPublisherDto) {
        Publisher publisherToUpdate = publisherRepository.findById(id)
            .orElseThrow(() -> new ResponseException(HttpStatus.NOT_FOUND,
                "Publisher with id " + id + " not found"));

        Address address = addressService.getById(creatingPublisherDto.getAddressId());

        publisherToUpdate.setName(creatingPublisherDto.getName());
        publisherToUpdate.setAddress(address);

        return publisherRepository.save(publisherToUpdate);
    }

    public void deleteById(Long id) {
        publisherRepository.deleteById(id);
    }

}

package ru.tinkoff.academy.elibrary.book;

import java.util.Map;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import ru.tinkoff.academy.elibrary.book.dto.CreatingBookDto;
import ru.tinkoff.academy.elibrary.book.specification.BookSearchSpecification;
import ru.tinkoff.academy.elibrary.common.exceptions.ResponseException;
import ru.tinkoff.academy.elibrary.genre.Genre;
import ru.tinkoff.academy.elibrary.genre.GenreService;
import ru.tinkoff.academy.elibrary.publisher.Publisher;
import ru.tinkoff.academy.elibrary.publisher.PublisherService;

@Service
@RequiredArgsConstructor
public class BookService {

    private final BookRepository bookRepository;
    private final GenreService genreService;
    private final PublisherService publisherService;

    public Book save(CreatingBookDto bookDto) {
        Publisher publisher = publisherService.getById(bookDto.getPublisherId());
        Genre genre = genreService.getById(bookDto.getGenreId());

        Book bookForSaving = Book.builder()
            .title(bookDto.getTitle())
            .publicationDate(bookDto.getPublicationDate())
            .publisher(publisher)
            .genre(genre)
            .build();

        return bookRepository.save(bookForSaving);
    }

    public Book getById(Long id) {
        return bookRepository.findById(id).orElseThrow(
            () -> new ResponseException(HttpStatus.NOT_FOUND, "Book with id " + id + " not found"));
    }

    public Book update(Long id, CreatingBookDto creatingBookDto) {
        Book bookToUpdate = bookRepository.findById(id)
            .orElseThrow(() -> new ResponseException(HttpStatus.NOT_FOUND,
                "Book with id " + id + " not found"));

        Genre genre = genreService.getById(creatingBookDto.getGenreId());
        Publisher publisher = publisherService.getById(creatingBookDto.getPublisherId());

        bookToUpdate.setTitle(creatingBookDto.getTitle());
        bookToUpdate.setGenre(genre);
        bookToUpdate.setPublisher(publisher);
        bookToUpdate.setPublicationDate(creatingBookDto.getPublicationDate());

        return bookRepository.save(bookToUpdate);
    }

    public void remove(Long bookId) {
        bookRepository.deleteById(bookId);
    }

    public Page<Book> getByPage(int pageNumber, int size, String field, String direction) {
        Sort sort = Sort.by(Direction.valueOf(direction), field);
        PageRequest pageable = PageRequest.of(pageNumber, size, sort);
        return bookRepository.findAll(pageable);
    }

    public Page<Book> findAllByParams(Map<String, String> params) {
        int pageNumber = Integer.parseInt(params.getOrDefault("page", "0"));
        params.remove("page");
        int pageSize = Integer.parseInt(params.getOrDefault("size", "10"));
        params.remove("size");

        Specification<Book> specification = BookSearchSpecification.searchByParams(params);
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        return bookRepository.findAll(specification, pageable);
    }

    public Page<Book> findAllByTitle(int pageNumber, int pageSize, String title) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        Specification<Book> specification = BookSearchSpecification.searchByTitle(title);
        return bookRepository.findAll(specification, pageable);
    }

    public Page<Book> findAllByAuthorAndGenre(int pageNumber, int pageSize, String author,
        String genre) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        Specification<Book> specification = Specification.where(
                BookSearchSpecification.searchByAuthor(author))
            .and(BookSearchSpecification.searchByGenre(genre));

        return bookRepository.findAll(specification, pageable);
    }
}

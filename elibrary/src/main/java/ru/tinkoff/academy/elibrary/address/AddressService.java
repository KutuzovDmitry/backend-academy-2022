package ru.tinkoff.academy.elibrary.address;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import ru.tinkoff.academy.elibrary.address.dto.CreatingAddressDto;
import ru.tinkoff.academy.elibrary.common.exceptions.ResponseException;

@Service
@RequiredArgsConstructor
public class AddressService {

    private final AddressRepository addressRepository;

    public Address save(CreatingAddressDto creatingAddressDto) {
        Address addressToCreate = Address.builder()
            .city(creatingAddressDto.getCity())
            .street(creatingAddressDto.getStreet())
            .build();

        return addressRepository.save(addressToCreate);
    }

    public Address getById(Long id) {
        return addressRepository.findById(id)
            .orElseThrow(() -> new ResponseException(HttpStatus.NOT_FOUND,
                "Address with id " + id + " not found"));
    }

    public Address update(Long id, CreatingAddressDto addressDto) {
        Address addressToUpdate = addressRepository.findById(id)
            .orElseThrow(() -> new ResponseException(HttpStatus.NOT_FOUND,
                "Address with id " + id + " not found"));

        addressToUpdate.setCity(addressDto.getCity());
        addressToUpdate.setStreet(addressDto.getStreet());

        return addressRepository.save(addressToUpdate);
    }

    public void deleteById(Long id) {
        addressRepository.deleteById(id);
    }

    public List<Address> getAll() {
        return addressRepository.findAll();
    }
}

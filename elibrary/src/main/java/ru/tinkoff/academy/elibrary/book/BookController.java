package ru.tinkoff.academy.elibrary.book;

import java.util.Map;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.tinkoff.academy.elibrary.book.dto.CreatingBookDto;

@RestController
@RequestMapping("/books")
@RequiredArgsConstructor
public class BookController {

    private final BookService bookService;

    @GetMapping("/search")
    public Page<Book> searchByParams(@RequestParam Map<String, String> params) {
        return bookService.findAllByParams(params);
    }

    @GetMapping("/search/by/title")
    public Page<Book> searchByTitle(
        @RequestParam(name = "page", required = false, defaultValue = "0") int page,
        @RequestParam(name = "size", required = false, defaultValue = "10") int size,
        @RequestParam String title) {
        return bookService.findAllByTitle(page, size, title);
    }

    @GetMapping("/search/by/genre/and/author")
    public Page<Book> searchByGenreAndAuthor(
        @RequestParam(name = "page", required = false, defaultValue = "0") int page,
        @RequestParam(name = "size", required = false, defaultValue = "10") int size,
        @RequestParam(name = "author", required = false, defaultValue = "") String author,
        @RequestParam(name = "genre", required = false, defaultValue = "") String genre
    ) {
        return bookService.findAllByAuthorAndGenre(page, size, author, genre);
    }

    @GetMapping
    public Page<Book> getByPage(@RequestParam(name = "page") int pageNumber,
        @RequestParam(name = "size") int size,
        @RequestParam(name = "field") String field,
        @RequestParam(name = "direction") String direction) {
        return bookService.getByPage(pageNumber, size, field, direction);
    }

    @PostMapping
    public Book save(@RequestBody CreatingBookDto creatingBookDto) {
        return bookService.save(creatingBookDto);
    }

    @GetMapping("/{id}")
    public Book getById(@PathVariable Long id) {
        return bookService.getById(id);
    }

    @PutMapping("/{id}")
    public Book update(@PathVariable Long id, @RequestBody CreatingBookDto creatingBookDto) {
        return bookService.update(id, creatingBookDto);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable Long id) {
        bookService.remove(id);
    }

}

package ru.tinkoff.academy.elibrary.relations;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.tinkoff.academy.elibrary.relations.dto.CreatingAuthorBookDto;

@RestController
@RequiredArgsConstructor
@RequestMapping("/authorbooks")
public class AuthorBookController {

    private final AuthorBookService authorBookService;

    @PostMapping()
    public AuthorBook save(@RequestBody CreatingAuthorBookDto creatingAuthorBookDto) {
        return authorBookService.save(creatingAuthorBookDto);
    }

    @GetMapping
    public List<AuthorBook> getAll() {
        return authorBookService.getAll();
    }
}

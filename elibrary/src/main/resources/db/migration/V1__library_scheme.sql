create table author(
    id bigserial primary key,
    name text not null
);

create table genre(
    id bigserial primary key,
    name text not null
);

create table address(
    id bigserial primary key,
    city text not null,
    street text not null
);

create table publisher(
    id bigserial primary key,
    name text not null,
    address_id bigint not null,
    foreign key(address_id) references address(id)
);

create table book(
    id bigserial primary key,
    title text not null,
    publication_date date not null,
    genre_id bigint not null,
    publisher_id bigint not null,

    foreign key(genre_id) references genre(id),
    foreign key(publisher_id) references publisher(id)
);

create table author_book(
    book_id bigint not null,
    author_id bigint not null,
    primary key(book_id, author_id),
    foreign key(book_id) references book(id),
    foreign key(author_id) references author(id)
);